const express = require("express");
const cors = require("cors");
const items = require("./app/items");
const mongoose = require('mongoose');
const app = express();
const port = 8000;

app.use(cors());
app.use(express.json());
app.use(express.static("public"));

const run = async () => {
  await mongoose.connect("mongodb://localhost/links", {useNewUrlParser: true});

  app.use("/", items());
  console.log("Connected to mongoDB");

  app.listen(port, () => {
    console.log(`Server started at http://localhost:${port}`);
  });
};

run().catch(console.log);