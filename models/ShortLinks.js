const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const LinkSchema = new Schema ({
    url: {
        type: String,
        required: true,
    },
    shortUrl: {
        type: String,
        required: true,
    },
});

const Link = mongoose.model("Link", LinkSchema);
module.exports = Link;