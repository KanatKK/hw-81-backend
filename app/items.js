const router = require("express").Router();
const Link = require('../models/ShortLinks');


const createRouter = () => {
    router.get("/link", async (req, res) => {
        try {
            const link = await Link.find();
            res.send(link);
        } catch (e) {
            res.status(500).send(e);
        }
    });

    router.get("/link/:id", async (req, res) => {
        const result = await Link.find({'shortUrl': req.params.id});
        if (result) {
            res.status(301).redirect(result[0].url);
        } else {
            res.sendStatus(400);
        }
    });

    router.post("/link", async (req, res) => {
        const linkData = req.body;
        const link = new Link(linkData);
        try {
            await link.save();
        } catch(e) {
            res.status(400).send(e);
        }
        res.send(link);
    });

    return router;
};

module.exports = createRouter;